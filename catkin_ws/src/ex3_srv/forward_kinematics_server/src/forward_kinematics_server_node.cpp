#include <ros/ros.h>

#include <moveit/robot_model_loader/robot_model_loader.h>
#include <moveit/robot_model/robot_model.h>
#include <moveit/robot_state/robot_state.h>

#include <moveit_msgs/GetPositionFK.h>
#include <Eigen/Geometry> 

#include <tf/tf.h>
#include <eigen_conversions/eigen_msg.h>

#include <moveit/robot_state/conversions.h>




bool forward_kinematics(moveit_msgs::GetPositionFK::Request &request, moveit_msgs::GetPositionFK::Response &response){

    robot_model_loader::RobotModelLoader robot_model_loader("robot_description");
    robot_model::RobotModelPtr kinematic_model = robot_model_loader.getModel();
    ROS_INFO("Model frame: %s", kinematic_model->getModelFrame().c_str());

    robot_state::RobotStatePtr kinematic_state(new robot_state::RobotState(kinematic_model));

    if(!moveit::core::robotStateMsgToRobotState(request.robot_state, *kinematic_state, false)){
        response.error_code.PLANNING_FAILED;
        return false;
    }

    const robot_state::JointModelGroup* joint_model_group = kinematic_model->getJointModelGroup("fanuc");

    kinematic_state->enforceBounds(joint_model_group);

    if(kinematic_state->satisfiesBounds(joint_model_group, 0.0)){
        const Eigen::Affine3d & ee_pose = kinematic_state->getGlobalLinkTransform(joint_model_group->getLinkModelNames().back());
        geometry_msgs::Pose pose_msg;
        tf::poseEigenToMsg(ee_pose, pose_msg);

        ROS_INFO_STREAM(pose_msg);

        geometry_msgs::PoseStamped pose_stamped_msgs;
        pose_stamped_msgs.pose = pose_msg;
        
        response.fk_link_names.push_back(joint_model_group->getLinkModelNames().back());
        response.pose_stamped.push_back(pose_stamped_msgs);
        response.error_code.val = 1;

        return true;
    }
    else{
        response.error_code.val = -1;
        return false;
    }

    return false;
}


int main(int argc, char **argv)
{
    ros::init(argc, argv, "forward_kinematics_server_node");

    ros::NodeHandle nh;
    ros::ServiceServer service = nh.advertiseService("GetPositionFK", forward_kinematics);
    
    ros::spin();
    return 0;
}