#include <ros/ros.h>

#include <moveit_msgs/GetPositionFK.h>
#include <moveit_msgs/RobotState.h>
#include <sensor_msgs/JointState.h>
#include <boost/bind.hpp>


void joint_states_callback(ros::NodeHandle &nh, const sensor_msgs::JointState::ConstPtr& msg){
    
    ros::ServiceClient client = nh.serviceClient <moveit_msgs::GetPositionFK>("GetPositionFK");
    moveit_msgs::GetPositionFK service;
    moveit_msgs::RobotState rs;
    moveit_msgs::MoveItErrorCodes err;
    err.val = 1;
    rs.joint_state = *msg;

    service.request.robot_state = rs;
    service.request.fk_link_names.push_back("flange");

    if (client.call(service)) {
        
        if(service.response.error_code.val == err.val){
            ROS_INFO_STREAM("================ Using interface ================");
            ROS_INFO_STREAM(service.response.fk_link_names[0]);
            ROS_INFO_STREAM(service.response.pose_stamped.back());

            

            client = nh.serviceClient <moveit_msgs::GetPositionFK>("/compute_fk");
            //moveit_msgs::GetPositionFK service_fk;

            //service_fk.request.robot_state = rs;
            //service_fk.request.fk_link_names.push_back("flange");

            service.response.pose_stamped.clear();
            
            if (client.call(service)) {
                ROS_INFO_STREAM("================ Using /compute_fk ================");

                ROS_INFO_STREAM(service.response.fk_link_names[0]);
                ROS_INFO_STREAM(service.response.pose_stamped.back());

            }else
            
            {
                ROS_ERROR("Error for /compute_fk");
            }
            
        }else

        {
            ROS_ERROR("Failed to compute pose");
        }
        
    }else {
        ROS_ERROR("Failed to call service GetPositionFK");
    }
    return;    
}


int main(int argc, char **argv) {
    ros::init(argc, argv, "forward_kinematics_client_node");
    
    ros::NodeHandle nh;
    ros::Subscriber sub = nh.subscribe<sensor_msgs::JointState>("joint_states", 1, boost::bind(&joint_states_callback, boost::ref(nh), _1));
    ros::spin();
    return 0;
}