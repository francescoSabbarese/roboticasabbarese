#include <moveit/move_group_interface/move_group_interface.h>
#include <moveit/planning_scene_interface/planning_scene_interface.h>
#include <moveit_msgs/DisplayRobotState.h>
#include <moveit_msgs/DisplayTrajectory.h>
#include <moveit_msgs/AttachedCollisionObject.h>
#include <moveit_msgs/CollisionObject.h>
#include <moveit_visual_tools/moveit_visual_tools.h>

#include <tf2/LinearMath/Quaternion.h>
#include <tf2/LinearMath/Scalar.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>

int main(int argc, char** argv)
{
    ros::init(argc, argv, "circular_trajectory_cartesian");
    ros::NodeHandle node_handle;
    ros::AsyncSpinner spinner(1);
    spinner.start();

    static const std::string PLANNING_GROUP = "fanuc";
    moveit::planning_interface::MoveGroupInterface move_group(PLANNING_GROUP);
    moveit::planning_interface::PlanningSceneInterface planning_scene_interface;
    const robot_state::JointModelGroup* joint_model_group = move_group.getCurrentState()->getJointModelGroup(PLANNING_GROUP);

    namespace rvt = rviz_visual_tools;
    moveit_visual_tools::MoveItVisualTools visual_tools("base_link");
    visual_tools.deleteAllMarkers();

    visual_tools.loadRemoteControl();

    Eigen::Isometry3d text_pose = Eigen::Isometry3d::Identity();
    text_pose.translation().z() = 1.75;
    visual_tools.publishText(text_pose, "MoveGroupInterface Demo", rvt::WHITE, rvt::XLARGE);

    visual_tools.trigger();

    ROS_INFO_NAMED("tutorial", "Planning frame: %s", move_group.getPlanningFrame().c_str());
    ROS_INFO_NAMED("tutorial", "End effector link: %s", move_group.getEndEffectorLink().c_str());
    ROS_INFO_NAMED("tutorial", "Available Planning Groups:");
    std::copy(move_group.getJointModelGroupNames().begin(), move_group.getJointModelGroupNames().end(),          std::ostream_iterator<std::string>(std::cout, ", "));
    visual_tools.prompt("Press 'next' in the RvizVisualToolsGui window to start the demo");

    move_group.setPlanningTime(500.0);
    move_group.allowReplanning(true);
    move_group.clearPoseTargets();
    move_group.clearTrajectoryConstraints();
    move_group.clearPathConstraints();



    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
    tf2Scalar yaw, pitch, roll;
    tf2::Quaternion quat_tf;
    geometry_msgs::Quaternion quat_msg;

    yaw = 0;
    pitch = 1.57079;
    roll = 0;

    quat_tf.setRPY(roll, pitch, yaw);

    quat_msg = tf2::toMsg(quat_tf);


    float angle_resolution = 0.5; 
    int points = int(360/angle_resolution);
    double r = 0.3;
    double angle = 0;

    geometry_msgs::Pose pose;
    std::vector<geometry_msgs::Pose> waypoints;   

    for(int i = 0; i < points; i++){

        angle+=angle_resolution;

        pose.orientation = quat_msg;

        pose.position.x =  1.010;
        pose.position.y = 0 + r*cos(angle);
        pose.position.z = 0.8 + r*sin(angle);

        waypoints.push_back(pose);

    }

    move_group.setMaxVelocityScalingFactor(0.5);

    moveit_msgs::RobotTrajectory trajectory;

    const double jump_threshold = 0.0;
    const double eef_step = 0.02;

    double fraction = move_group.computeCartesianPath(waypoints, eef_step, jump_threshold, trajectory);

    ROS_INFO_NAMED("tutorial", "Visualizing plan (Cartesian path) (%.2f%% acheived)", fraction * 100.0);


    visual_tools.deleteAllMarkers();
    visual_tools.publishText(text_pose, "Joint Space Goal", rvt::WHITE, rvt::XLARGE);
    visual_tools.publishPath(waypoints, rvt::LIME_GREEN, rvt::SMALL);
    
    for (std::size_t i = 0; i < waypoints.size(); ++i)
        visual_tools.publishAxisLabeled(waypoints[i], "pt" + std::to_string(i), rvt::SMALL);
    visual_tools.trigger();


    ros::Publisher plot_trajectory_publisher = node_handle.advertise<trajectory_msgs::JointTrajectoryPoint>("plot_planned_trajectory", 10000, true);

    ros::Duration sleep_time(0.05);

    for(int i=0; i < trajectory.joint_trajectory.points.size(); i++)
    {
        trajectory_msgs::JointTrajectoryPoint jtp = trajectory.joint_trajectory.points[i];

        plot_trajectory_publisher.publish(jtp);

        sleep_time.sleep();
    }
    ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

    
    ros::shutdown();
    return 0;
}

