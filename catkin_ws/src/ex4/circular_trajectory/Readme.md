# ex4

the trajectory is generated using the Cartesian path. The chosen parameters have completed the trajectory without running into joint limits

## How to run the planner

Running the planner needs the `robot_description` parameter be loaded on the parameter server. You can do it for the Comau fanuc by executing

```bash
roslaunch fanuc_moveit_config demo.launch
```

Run the planner with

```bash
rosrun circular_trajectory circular_trajectory
```

