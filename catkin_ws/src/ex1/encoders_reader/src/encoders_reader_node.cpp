#include <ros/ros.h>
#include <encoders_reader/encoders.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/String.h>

int main(int argc, char **argv) {
    
    ros::init(argc, argv, "encoders_reader_node");
    ros::NodeHandle nh;
    ros::Publisher pub = nh.advertise<encoders_reader::encoders>("joint_state", 10);
    
    ros::Rate loopRate(0.2);
    sensor_msgs::JointState  joint;
    int count = 1;
    int value = 0;

    while (ros::ok()){
        encoders_reader::encoders enc;
        
        for(int i = 1; i <= 6; i++){
            std::string s = std::to_string(i);
            value = i * count;
            joint.name = {"joint "+s};
            joint.position = {_Float64(value), _Float64(value), _Float64(value)};
            joint.velocity = {_Float64(value), _Float64(value)};
            joint.effort = {_Float64(value)};
            enc.encoders.push_back(joint) ;
        }
        ROS_INFO_STREAM(enc);
        pub.publish(enc);
        ros::spinOnce();
        loopRate.sleep();

        count++;

    }

    return 0;
}


