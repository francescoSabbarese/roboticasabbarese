#include <ros/ros.h>
#include <encoders_controller/encoders.h>
#include <sensor_msgs/JointState.h>
#include <std_msgs/String.h>

void joint_stateCallback(const encoders_controller::encoders& joint_state){

        ROS_INFO_STREAM(joint_state);
}

int main(int argc, char **argv){
    
    ros::init(argc, argv, "encoders_controller_node");
    ros::NodeHandle nh;

    ros::Subscriber sub = nh.subscribe("joint_state",10, joint_stateCallback);
    
    ros::spin();
    return 0;
}