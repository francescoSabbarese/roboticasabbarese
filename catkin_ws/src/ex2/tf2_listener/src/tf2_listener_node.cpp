#include <ros/ros.h>
#include <tf2_ros/transform_listener.h>
#include <geometry_msgs/TransformStamped.h>
#include <tf2/LinearMath/Quaternion.h>
#include <tf2_geometry_msgs/tf2_geometry_msgs.h>
#include <tf2/LinearMath/Matrix3x3.h>
#include <tf2/LinearMath/Vector3.h>
#include <tf2/LinearMath/Scalar.h>
#include <iostream>
#include<string>
using namespace std;


void print(string target_frame, string source_frame,  tf2_ros::Buffer *tfb){
    geometry_msgs::TransformStamped transformStamped;
    tf2::Quaternion quat_tf;
    geometry_msgs::Quaternion quat_msg;
    tf2::Vector3 temp;
    tf2::Vector3 translation;
    tf2::Vector3 axis;
    tf2Scalar yaw, pitch, roll, angle;
    string m;

    m = "<=================================== ";
    m = m + source_frame + " - ";
    m = m + target_frame + "===================================>\n\n";
    cout << m;
            
    transformStamped = tfb->lookupTransform(target_frame, source_frame, ros::Time(0));
    quat_msg = transformStamped.transform.rotation;

    tf2::convert(quat_msg , quat_tf);
    cout << "---------- TransformStamped ----------\n\n";

    ROS_INFO_STREAM(transformStamped);

    cout << "\n";
    cout << "---------- Translation Vector ----------\n\n";

    m = "[ ";
    m = m + to_string(transformStamped.transform.translation.x) + ", ";
    m = m + to_string(transformStamped.transform.translation.y) + ", ";
    m = m + to_string(transformStamped.transform.translation.z);
    m = m + " ]";

    cout << m;

    cout << "\n\n";
    cout << "---------- Rotation Matrix ----------\n\n";

    tf2::Matrix3x3 matrix(quat_tf);
            
    m = "[\n";

    for(int i=0; i<3; i++){
        temp = matrix.getRow(i);

        for(int j=0; j<3; j++){
            m = m + to_string(temp[j]) + ", ";
        }
        m = m + "\n";
    }

    m = m + "]\n";

    cout << m;

    cout << "\n";
    cout << "---------- Euler ----------\n\n";

    matrix.getEulerYPR(yaw, pitch, roll);

    m = "Yaw:\t" + to_string(yaw) + "\n";
    m = m + "Pitch:\t" + to_string(pitch) + "\n";
    m = m + "Roll:\t" + to_string(roll) + "\n";

    cout << m;

    cout << "\n";
    cout << "---------- Axis - Angle ----------\n\n";

    axis = quat_tf.getAxis();
    angle = quat_tf.getAngle();

    m = "Axis:\n";
    m = m + "\tx:\t" + to_string(axis[0]) + "\n";
    m = m + "\ty:\t" + to_string(axis[1]) + "\n";
    m = m + "\tz:\t" + to_string(axis[2]);

    m = m + "\n";
    cout << m;

    m = "Angle:\n\t" + to_string(angle);

    cout << m;

    cout << "\n";

    return;

}



int main(int argc, char** argv) {
    
    ros::init(argc, argv, "tf2_listener_node");
    ros::NodeHandle nodeHandle;
    tf2_ros::Buffer tfBuffer;
    tf2_ros::TransformListener tfListener(tfBuffer);

    //ogni 10 secondi 
    ros::Rate rate(0.08);

    string s_frame = "base_link";
    string t_frame = "flange";
    string frame = "link";

    while (nodeHandle.ok()) {

        try {

            print(t_frame, s_frame, &tfBuffer);

            for(int i=1; i<=6; i++){
                s_frame = frame + to_string(i);
                print(t_frame, s_frame, &tfBuffer);
            }

            s_frame = "base_link";

        } catch (tf2::TransformException &exception) {
            ROS_WARN("%s", exception.what());
            ros::Duration(1).sleep();
            continue;
        }
        rate.sleep();
    }
    return 0;
};